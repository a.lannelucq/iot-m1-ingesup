# Ibaby

## Présentation
Le projet a été réalisé pendant la majeure IoT d'Ingésup.

Les membres de ce projet sont :
* William BALANCE
* Arthur LANNELUCQ


## Démarrer le projet

Se placer dans le répertoire désiré et faire un git clone du projet :
```
    git clone https://gitlab.com/a.lannelucq/iot-m1-ingesup.git
    ...
    cd iot-m1-ingesup/
```

Une fois le projet récupéré, ouvrir 3 fenêtres dans un terminal. Chaque partie ci-dessous doit être fait dans une fenêtre différente.

### Serveur
Lancer le serveur sails puis choisir le premier mode (alter) pour conserver la base de donnée du projet.
```
	cd server/
	npm install
    npm start
```

### Arduino

Brancher la carte sur le pin A4.

```
	cd arduino/
	npm install
    npm start
```

### Mobile

```
	# Installer react-native-cli si pas installé
    npm install -g react-native-cli
    
	cd mobile/
	npm install
	react-native link
    
    # Pour démarrer le projet sur un simulateur iPhone
    # Xcode doit être installé pour pouvoir build sur iOs
    react-native run-ios
    
    # Pour démarrer le projet sur un simulateur Android
    # Android Studio doit être installé pour pouvoir build sur Android
    react-native run-android
```

En cas de problème pour build l'application, se référer à la documentation :
https://facebook.github.io/react-native/docs/getting-started.html

### Scénario

```
	* Register new account
	(OK) App qui lorsque je suis connecté est personnalisée à mon nom
	
	* Go to teams and choose one to apply
	(OK) la liste des joueur de ma team
	
	* Return to home, start match and choose a babyfoot
	(OK) la liste des baby foot avec leur dispo
	
	* Choose red/blue team
	* Go to arduino console and make a goal, it will start a match
	(OK) la possibilité de démarrer un match et y associé 2/4 joueurs
	
	* For finish this match, you must reach 10 goal or just cancel the match
	* Return to home, go to my matches and you can see it
	(OK) la page de détail d'un match avec la liste des joueur et le score
```