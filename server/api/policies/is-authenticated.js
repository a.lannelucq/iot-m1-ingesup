const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    const token = req.headers.jwt;

    if (!token) {
        return res.status(401).json({err: 'Request headers must contains token'})
    }

    jwt.verify(token, sails.config.jwt.jwtSecret, (err, decoded) => {
        if (err) {
          return res.status(401).json({err: 'Invalid token'})
        }
      next();
    });

};
