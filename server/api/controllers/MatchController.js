/**
 * MatchController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  current: (req, res) => {
    Match.find()
      .where({
        babyfoot: req.params.babyfoot,
        isFinished: false
      })
      .populate('players')
      .exec((err, match) => {
        Match.subscribe(req, match.map(r => r.id));

        match = match[0];
        User
          .find({
            id: match.players.map(p => p.user)
          })
          .exec((err, users) => {
            match.players.forEach((player,index) => {
              const indexU = users.findIndex((u => u.id === player.user));
              match.players[index].user = users[indexU];
            });
            res.json(match);
          })

      });
  }
};

