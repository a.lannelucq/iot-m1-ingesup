/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
  login: async (req, res) => {
    const email = req.param('email');
    const user = await User.findOne({email});

    if (!user) {
      return res.notFound();
    }

    let isIdentical = bcrypt.compareSync(req.param('password'), user.password);
    if (!isIdentical) {
      return res.notFound();
    }

    const {jwtSecret, jwtExpiresIn: expiresIn} = sails.config.jwt;
    const {id, firstName, lastName} = user;
    const token = jwt.sign({id, firstName, lastName, email}, jwtSecret, {expiresIn});
    return res.ok(token);
  },

  register: async (req, res) => {
    const email = req.param('email');
    const password = req.param('password');
    const {pseudo, lastName, firstName, birthDate, sex} = req.body;

    if (_.isUndefined(email) || _.isUndefined(password))
      return res.badRequest('Email and password must not be empty.');

    if (password.length < 5)
      return res.badRequest('Password must be longer. (min 5 characters)');

    const user = await sails.helpers.createUser(email, password, pseudo, lastName, firstName, sex, birthDate);
    const {jwtSecret, jwtExpiresIn: expiresIn} = sails.config.jwt;
    const token = jwt.sign({id: user.id, firstName, lastName, email}, jwtSecret, {expiresIn});

    return res.ok(token);
  },

  me: async (req, res) => {
    const {id} = jwt.decode(req.headers.jwt);
    const user = await User.findOne({id});
    return res.json({...user});
  }
};

