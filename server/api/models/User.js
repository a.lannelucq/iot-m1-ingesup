/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const bcrypt  = require('bcryptjs');

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    email: {
      type: 'string',
      unique: true,
      required: true,
      isEmail: true,
      maxLength: 250,
      example: 'user@mail.com'
    },
    pseudo: {
      type: 'string',
      //required: true,
      maxLength: 50,
      example: 'GROS KIKI DU 33'
    },
    lastName: {
      type: 'string',
      //required: true,
      maxLength: 50,
      example: 'BON'
    },
    firstName: {
      type: 'string',
      //required: true,
      maxLength: 50,
      example: 'Jean'
    },
    birthDate: {
      type: 'string',
      example: '30/03/1996'
    },
    sex: {
      type: 'string',
      isIn: ['male', 'female'],
      //required: true
    },
    password: {
      type: 'string',
      required: true
    },



    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    team: {
      model: 'team'
    },

    userMatches: {
      collection: 'usermatch',
      via: 'user'
    }
  }
};

