const bcrypt = require('bcryptjs');

module.exports = {


  friendlyName: 'Create user',


  description: 'Create new user',


  inputs: {
    email: {
      type: 'string'
    },
    password: {
      type: 'string'
    },
    pseudo: {
      type: 'string'
    },
    lastName: {
      type: 'string'
    },
    firstName: {
      type: 'string'
    },
    sex: {
      type: 'string'
    },
    birthDate: {
      type: 'string'
    }
  },


  exits: {
    invalid: {
      responseType: 'badRequest',
      description: 'Email and/or password not valid'
    },
    emailAlreadyInUse: {
      statusCode: 409,
      description: 'Email already in use'
    }
  },


  fn: async function (inputs, exits) {
    const attr = {...inputs};
    attr.email = inputs.email.toLowerCase();

    let user;
    if (inputs.password) {
      attr.password = await bcrypt.hash(inputs.password, 10);

      user = await User.create(attr)
        .intercept('E_UNIQUE', () => 'emailAlreadyInUse')
        //.intercept({name: 'UsageError'}, () => 'invalid')
        .fetch()
    }
    // All done.
    return exits.success(user);
  }


};

