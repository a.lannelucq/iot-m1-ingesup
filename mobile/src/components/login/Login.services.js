import {AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {JWT_KEY} from '../../constants';

const Environment = require('../../../environment.js');

export const parseJwt = async () => {
	const token = await AsyncStorage.getItem(JWT_KEY);
	let base64Url = token.split('.')[1];
	const base64 = base64Url.replace('-', '+').replace('_', '/');
	return JSON.parse(window.atob(base64));
};

export const login = async ({email, password}) => {
    const data = new FormData();
    data.append('email', email);
    data.append('password', password);

    const options = {
        method: 'POST',
        body: data
    };

    const response = await fetch(Environment.BASE_URL + '/api/user/login', options);
    return await response.json();
};

export const logout = async() => {
    await AsyncStorage.removeItem(JWT_KEY);
    Actions.reset('auth');
};