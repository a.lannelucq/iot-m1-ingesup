import React, {Component} from 'react';
import {View, AsyncStorage} from 'react-native';
import {H1, Container, Content, Form, Item, Input, Label, Text, Button} from 'native-base';
import {Actions} from 'react-native-router-flux';

// Services
import {login} from './Login.services';
import {JWT_KEY} from '../../constants';

export default class Login extends Component {

    state = {email: '', password: '', error: null};

    constructor() {
        super();
        console.disableYellowBox = true;
    }

    login = async () => {
        this.setState({error: null});
        try {
            const token = await login(this.state);
            await AsyncStorage.setItem(JWT_KEY, token);
            Actions.reset('main');
        } catch (err) {
            this.setState({error: 'Invalid email/password'});
        }
    };

    render() {
        const {buttonContainerStyle, buttonStyle, itemStyle, containerStyle, headerStyle} = styles;

        return (
            <Container style={containerStyle}>
                <Content>
                    <H1 style={headerStyle}> Please, login </H1>
                    <Form>
                        <Item floatingLabel style={itemStyle}>
                            <Label>Email</Label>
                            <Input type="email"
                                   onChangeText={email => this.setState({email})}
                                   value={this.state.email}
                                   autoCapitalize={'none'}/>
                        </Item>
                        <Item floatingLabel style={itemStyle}>
                            <Label>Password</Label>
                            <Input type="password"
								   secureTextEntry
                                   onChangeText={password => this.setState({password})}
                                   value={this.state.password}
                                   autoCapitalize={'none'}/>
                        </Item>
                        <Text>{this.state.error}</Text>
                        <View style={buttonContainerStyle}>
                            <Button bordered style={buttonStyle} onPress={this.login} title="Login">
                                <Text>Login</Text>
                            </Button>

                            <Button bordered style={buttonStyle} onPress={Actions.Register} title="Register">
                                <Text>Register</Text>
                            </Button>
                        </View>
                    </Form>
                </Content>
            </Container>
        );
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    buttonContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30
    },
    buttonStyle: {
        margin: 10
    },
    itemStyle: {
        marginHorizontal: 20
    },
    headerStyle: {
        marginTop: 20
    }
};
