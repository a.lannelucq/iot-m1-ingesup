import React, {Component} from 'react';
import {View, FlatList} from 'react-native';
import {H1} from 'native-base';
import HomeItem from './HomeItem';
import {parseJwt} from '../login/Login.services';
import {Spinner} from '../shared/Spinner';

export default class Home extends Component {
	
	state = {
		user: null
	};
	
	constructor() {
		super();
		this.menuItems = [
			{title: 'My matches', key: 'Matches', icon: 'star'},
			{title: 'My Team', key: 'Team', icon: 'group'},
			{title: 'My profile', key: 'ProfileDetail', icon: 'user-circle-o'},
			/*{title: 'My reservations', key: 'Reservations', icon: 'clock-o'},*/
			{title: 'Start new match', key: 'Babyfoots', icon: 'futbol-o'},
		];
        console.disableYellowBox = true;
	}
	
	async componentDidMount() {
		const user = await parseJwt();
		const {firstName, lastName} = user;
		
		this.setState({
			user: { firstName, lastName}
		})
	}
	
	render() {
		const {containerStyle, headerStyle} = styles;
		
		if (this.state.user === null) {
			return (
				<View style={containerStyle}>
					<Spinner/>
				</View>
			)
		}
		
		const {firstName, lastName} = this.state.user;
		
		return (
			<View style={containerStyle}>
				<H1 style={headerStyle}>{`Hello ${firstName} ${lastName} !`}</H1>
				<FlatList
					scrollEnabled={false}
					columnWrapperStyle={{marginHorizontal: 10}}
					contentContainerStyle={{marginTop: 20}}
					data={this.menuItems}
					renderItem={({item}) => <HomeItem screen={item}/>}
					numColumns={2}
				/>
			</View>
		);
	}
}

const styles = {
	containerStyle: {
		flex: 1,
		backgroundColor: 'white'
	},
	headerStyle: {
		margin: 20
	}
};
