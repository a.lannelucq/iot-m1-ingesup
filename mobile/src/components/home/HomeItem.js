import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Card, CardItem, Text, Body} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class HomeItem extends Component {
	render() {
		const {title, key, icon} = this.props.screen;
		const {cardStyle, cardItemStyle, iconStyle, textStyle} = styles;
		
		return (
			<TouchableOpacity style={{flex: 1}} onPress={() => Actions[key].call()}>
				<Card style={cardStyle}>
					<CardItem style={cardItemStyle}>
						<Body>
							<Icon style={[iconStyle]} active name={icon}/>
						</Body>
					</CardItem>
					<CardItem style={cardItemStyle}>
						<Body>
							<Text style={textStyle}>{title}</Text>
						</Body>
					</CardItem>
				</Card>
			</TouchableOpacity>
		);
	}
}

const styles = {
	cardStyle: {
		flex: 1,
		marginLeft: 7,
		marginRight: 7,
		borderRadius: 10
	},
	cardItemStyle: {
		borderRadius: 10,
		flex: 1
	},
	textStyle: {
		alignSelf: 'center'
	},
	iconStyle: {
		alignSelf: 'center',
		fontSize: 35,
		marginBottom: -10
	}
};
