import React, {Component} from 'react';
import {View, FlatList} from 'react-native';
import {H1, H2, Text, Button} from 'native-base';
import moment from 'moment';
import {Actions} from 'react-native-router-flux';

// Components
import ProfileItem from '../profile/ProfileItem';

// Services
import {parseJwt} from '../login/Login.services';
import {applyToTeam, leaveTeam} from './Team.service';


export default class TeamDetail extends Component {
	
	state = {
		teamContainsAuthUser : null	
	};
	
	async componentDidMount() {
		this.setState({
			teamContainsAuthUser : await this.isAuthUserInTeam()
		});
	}
	async isAuthUserInTeam() {
		const {members} = this.props.team;
		const authenticatedUserMail = (await parseJwt()).email;
		return members.filter(member => member.email === authenticatedUserMail).length > 0;
	}

	applyToTeam = async () => {
		await applyToTeam(this.props.team);
		Actions.reset('main');
	};

	leaveTeam = async () => {
		await leaveTeam(this.props.team);
        Actions.reset('main');
	};
	
	renderButton() {
		const {buttonStyle} = styles;
		
		if (this.state.teamContainsAuthUser) {
			return (
				<Button onPress={this.leaveTeam} bordered danger style={buttonStyle}>
					<Text style={{color: 'red'}}> Leave Team</Text>
				</Button>
			);
		}
		
		return (
			<Button onPress={this.applyToTeam} bordered info style={buttonStyle}>
				<Text>Join Team</Text>
			</Button>
		);
	}
	
	render() {
		const {name, createdAt, members} = this.props.team;
		const {containerStyle, headerStyle} = styles;
		const creationDate = moment(createdAt).format('LL');
		
		return (
			<View style={containerStyle}>
				
				<View style={headerStyle}>
					<H1>{name}</H1>
					<Text>Created on {creationDate}</Text>
					{this.renderButton()}
				</View>
				<View style={{marginTop: 20}}>
					<H2> Members : </H2>
					<FlatList
						data={members}
						renderItem={({item}) => <ProfileItem user={item}/>}
						keyExtractor={item => item.email}
					/>
				</View>
			</View>
		);
	}
}

const styles = {
	containerStyle: {
		flex: 1,
		backgroundColor: 'white',
	},
	headerStyle: {
		marginTop: 20,
		alignItems: 'center'
	},
	buttonStyle: {
		alignSelf: 'center',
		marginTop: 20
	}
};
