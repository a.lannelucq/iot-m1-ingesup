import React, {Component} from 'react';
import {View} from 'native-base';
import TeamList from './TeamList';
import {getAuthenticatedUser} from '../profile/Profile.service';
import {Spinner} from '../shared/Spinner';
import TeamDetail from './TeamDetail';
import Environment from '../../../environment';

export default class Team extends Component {
	state = {
		load: true,
		userTeam: null
	};
	
	async componentDidMount() {
		const {team: teamId} = await getAuthenticatedUser();
		
		if (teamId !== null) {
			const team = await (await fetch(`${Environment.BASE_URL}/team/${teamId}`)).json();
			this.setState({userTeam: team, load: false});
		}
		this.setState({load: false});
	}
	
	renderTeam() {
		const {load, userTeam} = this.state;
		
		if (load) {
			return <Spinner/>
		} else if (userTeam !== null) {
			return <TeamDetail team={userTeam}/>
		}
		return <TeamList/>
	}
	
	render() {
		return (
			<View style={styles.containerStyle}>
				{this.renderTeam()}
			</View>
		);
	}
}

const styles = {
	containerStyle: {
		flex: 1,
		backgroundColor: 'white'
	}
};
