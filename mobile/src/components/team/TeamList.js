import React, {Component} from 'react';
import {AsyncStorage, FlatList, View} from 'react-native';
import TeamItem from './TeamItem';
import SailsSocket from 'sails-socket';
import {JWT_KEY} from '../../constants';
import _ from 'lodash';

export default class TeamList extends Component {
	
	state = {
		teams: []
	};
	
	async componentDidMount() {
		const token = await AsyncStorage.getItem(JWT_KEY);
		SailsSocket.setHeader({jwt: token});
		
		SailsSocket.get('/team').then(jwr => {
			this.setState({teams: jwr.body})
		});
		
		
		SailsSocket.on('team', response => {
			let teams = this.state.teams;
			const {data: team} = response;
			
			if (response.verb === 'created') {
				this.addTeam(team)
			} else if (response.verb === 'updated') {
				this.modifyTeamFrom(team, teams)
			}
		});
	}
	
	addTeam(team) {
		this.setState(prevState => [...prevState, team])
	}
	
	modifyTeamFrom(teamSocket, teams) {
		index = _.findIndex(teams, team => team.id === teamSocket.id);
		
		if (index !== -1) {
			const newTeams = [...teams];
			newTeams[index] = _.extend(newTeams[index], teamSocket);
			this.setState({teams: newTeams})
		}
	}
	
	render() {
		return (
			
			<View>
				<FlatList
					data={this.state.teams}
					keyExtractor={item => item.name}
					renderItem={({item}) => <TeamItem team={item}/>}
				/>
			</View>
		);
	}
}
