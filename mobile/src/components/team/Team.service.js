const Environment = require('../../../environment.js');
import {parseJwt} from '../login/Login.services';

export const fetchAllTeams = async () => {
    const url = 'http://localhost:1337/team';
    return await (await fetch(url)).json();
};

export const applyToTeam = async (team) => {
    const jwt = await parseJwt();
    const data = new FormData();
    team.members.forEach(m => data.append('members', m.id));
    data.append('members', jwt.id);

    const options = {
        method: 'PUT',
        body: data
    };

    const response = await fetch(Environment.BASE_URL + '/team/' + team.id, options);
    const json = await response.json();

    if (response.ok) return json;
    throw json.details || json.code;
};

export const leaveTeam = async (team) => {
    const jwt = await parseJwt();
    const data = new FormData();
    team.members.forEach(m => jwt.id !== m.id ? data.append('members', m.id) : '');

    const options = {
        method: 'PUT',
        body: data
    };

    const response = await fetch(Environment.BASE_URL + '/team/' + team.id, options);
    const json = await response.json();

    if (response.ok) return json;
    throw json.details || json.code;
};