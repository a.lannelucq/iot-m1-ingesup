import React, {Component} from 'react';
import {Text, ListItem, Thumbnail, Left, Right, Body} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';

export default class TeamItem extends Component {
	
	render() {
		const {team} = this.props;
		const {name, imageUrl} = team;
		
		const uri = imageUrl ? imageUrl : 'https://svn.alfresco.com/repos/alfresco-open-mirror/alfresco/HEAD/root/projects/repository/config/alfresco/thumbnail/thumbnail_placeholder_256_ai.png';
		return (
			<ListItem button onPress={() => {Actions.TeamDetail({team})}}>
				<Left>
					<Thumbnail source={{uri}} square/>
				</Left>
				<Body>
					<Text>{name}</Text>
				</Body>
				<Right>
					<Icon name='angle-right' size={20}/>
				</Right>
			</ListItem>
		);
	}
}
