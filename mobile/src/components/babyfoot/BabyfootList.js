import React, {Component} from 'react';
import {View, FlatList, AsyncStorage} from 'react-native';
import {ListItem, Text, Left, Right} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {Spinner} from '../shared/Spinner';
import Icon from 'react-native-vector-icons/FontAwesome';
import SailsSocket from 'sails-socket';
import _ from 'lodash';

export default class BabyfootList extends Component {
	
	state = {
		babyfoots: null
	};
	
	async componentDidMount() {
		SailsSocket.get('/babyfoot').then(({body: babyfoots}) => this.setState({babyfoots}))
		
		SailsSocket.on('babyfoot', response => {
			if (response.verb === 'updated') {
				this.setState(prevState => {
					const index = _.findIndex(prevState.babyfoots, babyfoot => babyfoot.id === response.data.id);
					
					if (index !== -1) {
						const updatedBabyfoots = [...prevState.babyfoots];
						updatedBabyfoots[index].available = response.data.available;
						return {babyfoots: updatedBabyfoots};
					}
				});
			}
		})
		
	}
	
	renderAvailableIcon(isAvailable) {
		if (!isAvailable)
			return <Icon name='circle' size={20} color='red' style={{marginRight: 10}}/>;
		else
            return <Icon name='circle' size={20} color='green' style={{marginRight: 10}}/>;
	}
	
	renderBabyfootItem(babyfoot) {
		
		return (
			<ListItem button onPress={() => {
				Actions.StartMatch({babyfoot})
			}}>
				<Left>
                    {this.renderAvailableIcon(babyfoot.available)}
					<Text>{`Location : ${babyfoot.place}`}</Text>
				</Left>
				<Right>
					<Icon name='angle-right' size={20}/>
				</Right>
			</ListItem>
		);
	}
	
	render() {
		const {babyfoots} = this.state;
		
		if (babyfoots === null) {
			return <Spinner/>
		}
		
		return (
			<View style={styles.containerStyle}>
				<FlatList
					data={babyfoots}
					keyExtractor={item => item.place}
					renderItem={({item}) => this.renderBabyfootItem(item)}
				/>
			</View>
		);
	}
}

const styles = {
	containerStyle: {
		flex: 1,
		backgroundColor: 'white'
	}
}
