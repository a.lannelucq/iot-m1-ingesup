import React, { Component } from 'react';
import {ListItem, Text, Left, Right} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';

export default class ProfileItem extends Component {
    render() {
    	const {user} = this.props;
    	const {lastName, firstName} = user;
		
		return(
			<ListItem button onPress={() => {Actions.ProfileDetail({user})}}>
				<Left>
					<Text>{`${firstName} ${lastName}`}</Text>
				</Left>
				<Right>
					<Icon name='angle-right' size={20}/>
				</Right>
			</ListItem>
		);
    };
}