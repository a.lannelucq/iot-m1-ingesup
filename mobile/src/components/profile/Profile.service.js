import {AsyncStorage} from 'react-native';
import {JWT_KEY} from '../../constants'
const Environment = require('../../../environment.js');

export const getAuthenticatedUser = async () => {
	const token = await AsyncStorage.getItem(JWT_KEY);
	const headers = new Headers();
	headers.append('Content-type', 'application/json');
	headers.append('jwt', token);
	
	const options = {
		method: 'GET',
		headers
	};
	
	return await (await fetch(Environment.BASE_URL + '/api/user/me', options)).json();
};