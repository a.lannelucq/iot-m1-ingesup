import React, {Component} from 'react';
import {View} from 'react-native';
import {H1, Thumbnail, Text} from 'native-base';
import {getAuthenticatedUser} from './Profile.service';
import {Spinner} from '../shared/Spinner';

export default class ProfileDetail extends Component {
	
	state = {
		user: null
	};
	
	async componentDidMount() {
		let user = this.props.user;
		if (!user) {
			user = await getAuthenticatedUser();
		}
		
		this.setState({user});
	}
	
	render() {
		const {containerStyle, headerStyle, mailStyle} = styles;
		
		if (this.state.user === null) {
			return (
				<View style={containerStyle}>
					<Spinner/>
				</View>
			);
		}
		
		const {lastName, firstName, imageUrl, email} = this.state.user;
		const uri = imageUrl ? imageUrl : 'https://svn.alfresco.com/repos/alfresco-open-mirror/alfresco/HEAD/root/projects/repository/config/alfresco/thumbnail/thumbnail_placeholder_256_ai.png';
		
		return (
			<View style={containerStyle}>
				<View style={headerStyle}>
					<Thumbnail source={{uri}} square/>
					<H1>{`${firstName} ${lastName}`}</H1>
					<Text style={mailStyle}>{email}</Text>
				</View>
			</View>
		);
	}
}

const styles = {
	containerStyle: {
		flex: 1,
		backgroundColor: 'white'
	},
	headerStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 20
	},
	mailStyle: {
		fontSize: 12,
		color: 'grey'
	}
};