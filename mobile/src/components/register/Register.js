import React, {Component} from 'react';
import {AsyncStorage} from 'react-native';
import {Container, Content, Form, Item, Input, Label, Text, H1, Button} from 'native-base';
import {Actions} from 'react-native-router-flux';

// Services
import {register} from './Register.services';
import {JWT_KEY} from '../../constants';

export default class Login extends Component {

    state = {
        email: 'test@test.com',
        password: 'password',
        pseudo: 'Willyboat',
        firstName: 'William',
        lastName: 'Balance',
        sex: 'male',
        birthDate: '30/03/1996',
        error: null
    };

    register = async () => {
        this.setState({error: null});
        try {
            const token = await register(this.state);
            await AsyncStorage.setItem(JWT_KEY, token);
            Actions.reset('main');
        } catch(error) {
            this.setState({error});
        }

    };

    render() {
		const {buttonStyle, itemStyle, containerStyle, headerStyle} = styles;
	
		return (
            <Container style={containerStyle}>
                <Content>
                    <H1 style={headerStyle}>Welcome to our app ! </H1>
                    <Form>
                        <Item floatingLabel style={itemStyle}>
                            <Label>Email</Label>
                            <Input type="email"
                                   onChangeText={email => this.setState({email})}
                                   value={this.state.email}
                                   autoCapitalize={'none'}/>
                        </Item>
                        <Item floatingLabel style={itemStyle}>
                            <Label>Password</Label>
                            <Input type="password"
								   secureTextEntry
                                   onChangeText={password => this.setState({password})}
                                   value={this.state.password}
                                   autoCapitalize={'none'}/>
                        </Item>
                        <Item floatingLabel style={itemStyle}>
                            <Label>Pseudo</Label>
                            <Input type="text"
                                   onChangeText={pseudo => this.setState({pseudo})}
                                   value={this.state.pseudo}/>
                        </Item>
                        <Item floatingLabel style={itemStyle}>
                            <Label>FirstName</Label>
                            <Input type="text"
                                   onChangeText={firstName => this.setState({firstName})}
                                   value={this.state.firstName}/>
                        </Item>
                        <Item floatingLabel style={itemStyle}>
                            <Label>LastName</Label>
                            <Input type="text"
                                   onChangeText={lastName => this.setState({lastName})}
                                   value={this.state.lastName}/>
                        </Item>
                        <Item floatingLabel style={itemStyle}>
                            <Label>Sex</Label>
                            <Input type="text"
                                   onChangeText={sex => this.setState({sex})}
                                   value={this.state.sex}/>
                        </Item>
                        <Item floatingLabel style={itemStyle}>
                            <Label>BirthDate</Label>
                            <Input type="text"
                                   onChangeText={birthDate => this.setState({birthDate})}
                                   value={this.state.birthDate}/>
                        </Item>
                        <Text>{this.state.error}</Text>
                        <Button style={buttonStyle} bordered onPress={this.register} title="Register">
                            <Text>Register</Text>
                        </Button>
                    </Form>
                </Content>
            </Container>
        );
    }
}

const styles = {
	containerStyle: {
		flex: 1,
		backgroundColor: 'white'
	},
	buttonStyle: {
		margin: 30,
        alignSelf: 'center'
	},
	itemStyle: {
		marginHorizontal: 20
	},
	headerStyle: {
		marginVertical: 15,
        marginLeft: 10
	}
};