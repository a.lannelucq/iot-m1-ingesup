const Environment = require('../../../environment.js');

export const register = async (user) => {
    const data = new FormData();
    data.append('email', user.email);
    data.append('password', user.password);
    data.append('pseudo', user.pseudo);
    data.append('firstName', user.firstName);
    data.append('lastName', user.lastName);
    data.append('sex', user.sex);
    data.append('birthDate', user.birthDate);

    const options = {
        method: 'POST',
        body: data
    };

    const response = await fetch(Environment.BASE_URL + '/api/user/register', options);
    const json = await response.json();

    if(response.ok)
        return json;
    else
        throw json.details || json.code;
};