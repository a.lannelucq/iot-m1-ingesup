import React, {Component} from 'react';
import {View, AsyncStorage} from 'react-native';
import {Router, Scene} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import SailsSocket from 'sails-socket';

// Components
import Home from './home/Home';
import Matches from './matches/Matches';
import Team from './team/Team';
import TeamDetail from './team/TeamDetail';
import Login from './login/Login';
import ProfileDetail from './profile/ProfileDetail';
import Register from './register/Register';
import StartMatch from './matches/StartMatch';
import {logout} from './login/Login.services';

// Constants
import {JWT_KEY} from '../constants';
import BabyfootList from './babyfoot/BabyfootList';


class RouterComponent extends Component {

    state = {
        jwtExists: null
    };

    async componentDidMount() {
        const token = await AsyncStorage.getItem(JWT_KEY);
        const jwtExists = token !== null;
        this.setState({jwtExists});
	
		const initParams = {url: 'http://localhost:1337'};
		sailSocket = SailsSocket.connect(initParams);
		sailSocket.sails.useCORSRouteToGetCookie = false;
	}

    render() {
        const {jwtExists} = this.state;

        if (jwtExists === null) {
            return <View></View>
        }

        return (
            <Router>
                <Scene key='main' hideNavBar>
                    <Scene key='auth' initial={!jwtExists}>
                        <Scene key='Login' component={Login} title='Login' initial/>
                        <Scene key='Register' component={Register} title='Register'/>
                    </Scene>

                    <Scene key='main' initial={jwtExists}>
                        <Scene key='Home' component={Home} title='Home' initial/>
                        <Scene key='Team' component={Team} title='Teams'/>
                        <Scene key='TeamDetail' component={TeamDetail} title='Team detail'/>
                        <Scene key='ProfileDetail'
                               component={ProfileDetail}
                               title='Profile'
							   rightTitle={<Icon size={20} name='sign-out' color='black'/>}
							   onRight={logout}/>
                        <Scene key='Matches' component={Matches} title='Matches'/>
                        <Scene key='Babyfoots' component={BabyfootList} title='Select babyfoot' />
						<Scene key='StartMatch' component={StartMatch} title='Start new match'/>
                    </Scene>
                </Scene>
            </Router>
        );
    }
}

export default RouterComponent;