import React, {Component} from 'react';
import {Container, Content, List, ListItem, Text, Icon, Left, Body, Right} from 'native-base';

// Services
import {getMatches} from './Matches.services';

export default class Matches extends Component {

    state = {matches: []};

    async componentWillMount() {
        const matches = await getMatches();
        this.setState({matches});
    };

    formatDuration = (durationInMs) => {
        const minutes = Math.floor(durationInMs / 60000);
        const seconds = ((durationInMs % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    };

    render() {
        return (
            <Container>
                <Content>
                    {
                        this.state.matches.map(match =>
                            <List key={match.id}>
                                <ListItem icon>
                                    <Left>
                                        <Icon name="trophy"/>
                                    </Left>
                                    <Body>
                                    <Text>{match.redScore + ' - ' + match.blueScore}</Text>
                                    </Body>
                                    <Right>
                                        <Text>{this.formatDuration(match.duration)}</Text>
                                    </Right>
                                </ListItem>
                            </List>)
                    }
                </Content>
            </Container>
        );
    }
}