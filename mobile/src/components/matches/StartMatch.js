import React, {Component} from 'react';
import {AsyncStorage, View, Alert} from 'react-native';
import {Container, Button, Text, H1} from 'native-base';
import TeamMatch from './TeamMatch';
import Stopwatch from './Stopwatch';
import {JWT_KEY} from '../../constants';
import SailsSocket from 'sails-socket';
import _ from 'lodash';
import {Actions} from 'react-native-router-flux';

import Environment from '../../../environment';
import {getAuthenticatedUser} from '../profile/Profile.service';
import {Spinner} from '../shared/Spinner';

export default class StartMatch extends Component {

    state = {
        isFinished: false,
        isInProgress: null,
        blueTeam: null,
        redTeam: null,
        blueScore: null,
        redScore: null

    };

    async componentDidMount() {

        this.currentUser = await getAuthenticatedUser();

        const token = await AsyncStorage.getItem(JWT_KEY);
        const headers = new Headers();
        headers.append('jwt', token);
        this.jwt = token;
        this.headers = headers;

        SailsSocket.setHeader({jwt: this.jwt});

        const {id: babyfootId} = this.props.babyfoot;
        SailsSocket.get(`/api/match/current?babyfoot=${babyfootId}`).then(jwr => {
            const {players, id, blueScore, redScore, isInProgress} = jwr.body;
            this.matchId = id;

            const blueTeam = _.filter(players, ['color', 'blue']).map(({user}) => user);
            const redTeam = _.filter(players, ['color', 'red']).map(({user}) => user);
            this.setState({blueTeam, redTeam, blueScore, redScore, isInProgress});
        });

        SailsSocket.on('match', async response => {
            const {verb, attribute, addedId, data} = response;

            if (verb === 'addedTo' && attribute === 'players') {
                const userMatch = await (await fetch(`${Environment.BASE_URL}/usermatch/${addedId}`)).json();
                this.setState(prevState => {
                    return {
                        blueTeam: userMatch.color === 'blue' ? [...prevState.blueTeam, userMatch.user] : prevState.blueTeam,
                        redTeam: userMatch.color === 'red' ? [...prevState.redTeam, userMatch.user] : prevState.redTeam,
                    }
                })
            } else if (verb === 'updated') {
                this.setState(prevState => {
                    return {
                        redScore: data.redScore ? data.redScore : prevState.redScore,
                        blueScore: data.blueScore ? data.blueScore : prevState.blueScore,
                        isFinished: data.isFinished ? data.isFinished : prevState.isFinished,
                        isInProgress: data.isInProgress ? data.isInProgress : prevState.isInProgress
                    }
                })
            }
        });
    }

    addUserInTeam = async (teamColor) => {
        const token = await AsyncStorage.getItem(JWT_KEY);

        const headers = new Headers();
        headers.append('jwt', token);

        const body = new FormData();
        body.append('match', this.matchId);
        body.append('user', this.currentUser.id);
        body.append('color', teamColor);

        await fetch(`${Environment.BASE_URL}/usermatch`, {method: 'POST', body, headers});
    };

    stopMatch = async () => {
        const token = await AsyncStorage.getItem(JWT_KEY);
        const headers = new Headers();
        headers.append('jwt', token);

        const body = new FormData();
        body.append('isFinished', true);

        const options = {method: 'PUT', body, headers};

        await fetch(`${Environment.BASE_URL}/match/${this.matchId}`, options);
        Actions.reset('main');
    };

    confirm = () => {
        Alert.alert(
            'Would you stop the match ?',
            '',
            [
                {
                    text: 'No', onPress: () => {
                    }
                },
                {text: 'Stop match', onPress: this.stopMatch, style: 'destructive'},
            ]
        )
    };

    renderMatch() {
        const {scoreTextStyle, stopButtonStyle} = styles;
        const {blueScore, redScore, isFinished} = this.state;

        if (isFinished) {
            return <Text style={scoreTextStyle}>{`${blueScore} - ${redScore}`}</Text>;
        }

        return (
            <View>
                <Stopwatch/>
                <Text style={scoreTextStyle}>{`${blueScore} - ${redScore}`}</Text>
                <Button danger style={stopButtonStyle} onPress={this.confirm}>
                    <Text> Stop match </Text>
                </Button>
            </View>
        );
    }

    renderButton = () => {

        if (this.state.isInProgress) {
            return this.renderMatch();
        }
        const {blueTeam, redTeam} = this.state;
        const teamsContainsCurrentUser = [...blueTeam, ...redTeam].filter(user => user.email === this.currentUser.email).length !== 0

        return (
            <View style={{flex: 1}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                    <Button
                        style={{margin: 10}}
                        info
                        disabled={redTeam.length === 2 || teamsContainsCurrentUser}
                        onPress={() => this.addUserInTeam('blue')}
                    >
                        <Text> Join blue team</Text>
                    </Button>
                    <Button
                        style={{margin: 10}}
                        danger
                        disabled={redTeam.length === 2 || teamsContainsCurrentUser}
                        onPress={() => this.addUserInTeam('red')}
                    >
                        <Text> Join red team</Text>
                    </Button>
                </View>
            </View>
        );
    };

    render() {
        const {containerStyle, locationTextStyle} = styles;
        const {blueTeam, redTeam} = this.state;

        if (blueTeam === null && redTeam === null) {
            return (
                <Container style={styles.containerStyle}>
                    <Spinner/>
                </Container>
            )
        }
        return (
            <Container style={containerStyle}>
                <H1 style={locationTextStyle}> Location : {this.props.babyfoot.place}</H1>
                <TeamMatch blueTeam={blueTeam} redTeam={redTeam}/>
                {this.renderButton()}
            </Container>
        );
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    locationTextStyle: {
        alignSelf: 'center',
        marginTop: 20
    },
    scoreTextStyle: {
        fontSize: 70,
        marginTop: 30,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    stopButtonStyle: {
        alignSelf: 'center',
        marginTop: 50
    }
};