import React, { Component } from 'react';
import {View, Text, FlatList} from 'react-native';

export default class TeamMatch extends Component {
	
	renderTeamMember({email, firstName, lastName}) {
		return <Text key={email} style={styles.memberStyle}>{`${firstName} ${lastName}`}</Text>
	}
	
	renderTeam(team, teamColor) {
		
		const color = teamColor === 'Blue' ? '#08B2E3' : '#EE6352';
		const {matchTeamStyle, teamTextStyle} = styles;
		
		return (
			<View style={matchTeamStyle}>
				<Text style={[teamTextStyle, {color}]}> {teamColor} Team</Text>
				<FlatList
					data={team}
					renderItem={({item}) => this.renderTeamMember(item)}
					keyExtractor={item => item.email}
				/>
			</View>
		)
	}
	
    render() {
		const {redTeam, blueTeam} = this.props;
		
		return (
			<View style={styles.matchTeamsContainerStyle}>
				{this.renderTeam(blueTeam, 'Blue')}
				{this.renderTeam(redTeam, 'Red')}
			</View>
		);
    }
}

const styles = {
	matchTeamsContainerStyle: {
		flexDirection: 'row',
		alignContent: 'space-between',
		marginVertical: 10
	},
	matchTeamStyle: {
		flex: 1,
		alignItems: 'center'
	},
	teamTextStyle: {
		fontSize: 24,
		marginVertical: 20
	},
	memberStyle: {
		fontWeight: 'bold',
		margin: 5
	}
};
