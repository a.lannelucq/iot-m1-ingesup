import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Button} from 'native-base';
import _ from 'lodash';

export default class Stopwatch extends Component {
	
	state = {
		seconds: '00',
		minutes: '00'
	};
	
	componentDidMount() {
		this.loop = setInterval(() => this.incrementTimer(), 1000)
	}
	
	incrementTimer() {
		let {seconds, minutes} = this.state;
		
		if (seconds == 59) {
			minutes++;
			seconds = -1;
		}
		seconds++;
		
		this.setState({
			seconds: _.padStart(seconds, 2, '0'),
			minutes: _.padStart(minutes, 2, '0')
		});
	}
	
	render() {
		const {seconds, minutes} = this.state;
		
		return (
			<View style={{alignSelf: 'center'}}>
				<Text style={styles.textStyle}>{`${minutes} : ${seconds}`}</Text>
			</View>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 24,
		fontWeight: 'bold'
	}
};
