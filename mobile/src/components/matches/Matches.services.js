const Environment = require('../../../environment.js');

export const getMatches = async () => {
    const response = await fetch(Environment.BASE_URL + '/match');
    return await response.json();
};