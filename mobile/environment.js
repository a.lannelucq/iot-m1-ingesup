const _Environments = {
    local: {BASE_URL: 'http://localhost:1337'},
    development: {BASE_URL: ''}
};

function getEnvironment() {
    return _Environments['local'];
}

const Environment = getEnvironment();
module.exports = Environment;