import React, {Component} from 'react';
import {View} from 'react-native';
import Router from './src/components/Router';

export default class App extends Component {
	render() {
		return (
			<View style={styles.containerStyle}>
				<Router/>
			</View>
		);
	}
}

const styles = {
	containerStyle: {
		flex: 1,
		alignSelf: 'stretch'
	}
};