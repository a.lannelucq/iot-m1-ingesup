const five = require("johnny-five");
const board = new five.Board();
const io = require('sails.io.js')(require('socket.io-client'));
io.sails.url = 'http://localhost:1337';

let timeout = null;
const setTimeoutToSetNewGoal = (match) => {
    if (timeout) return;

    match.goal();
    timeout = setTimeout(() => {
        timeout = null;
    }, 1000);
};

board.on('ready', () => {
    const sensor = new five.Sensor({
        pin: "A4"
    });
    const match = new Match(1);

    sensor.within([0, 10], () => {
        setTimeoutToSetNewGoal(match);
    });
});

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const askForGoal = () => {
    rl.question('Write g to score a goal : \n', (answer) => {
        if(answer === 'g')
            setTimeoutToSetNewGoal(match);
        askForGoal();
    });
};

const request = require('request');

const hasError = (error) => {
    if (error) {
        console.log('Error');
        console.log(error);
        return true;
    }
    return false;
};

const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
};

class Match {

    constructor(babyId) {
        this.babyId = babyId;
        this.init(null);
        this.initNextMatch()
            .then(() => askForGoal());
    }

    init(id) {
        this.scoreBlue = 0;
        this.scoreRed = 0;
        this.id = id;
        this.duration = 0;
        this.isInProgress = false;
    }

    goal() {
        if (!this.isInProgress) {
            this.startMatch();
            return;
        }
        getRandomInt(2) === 0 ? this.scoreBlue++ : this.scoreRed++;
        const options = {
            method: 'PUT',
            url: `http://localhost:1337/match/${this.id}`,
            form: {
                redScore: this.scoreRed,
                blueScore: this.scoreBlue
            }
        };

        return new Promise((resolve, reject) => {
            request(options, error => {
                if (hasError(error)) reject(error);
                this.hasWinner().then(() => resolve());
            });
        });
    }

    async hasWinner() {
        if (this.scoreBlue >= 10 || this.scoreRed >= 10) {
            console.log('Has Winner');
            await this.matchIsFinished();
            return await this.createNewMatch();
        }
    }

    matchIsFinished() {
        const duration = new Date().getTime() - this.duration;
        const options = {
            method: 'PUT',
            url: `http://localhost:1337/match/${this.id}`,
            form: {
                isFinished: true,
                duration: duration
            }
        };

        return new Promise((resolve, reject) => {
            request(options, error => {
                if (hasError(error)) reject(error);
                console.log('Match is finished');
                resolve();
            });
        });
    }

    createNewMatch() {
        const options = {
            method: 'POST',
            url: `http://localhost:1337/match`,
            form: {
                isInProgress: false,
                isFinished: false,
                babyfoot: this.babyId
            }
        };

        return new Promise((resolve, reject) => {
            request(options, (error, res, body) => {
                if (hasError(error)) reject(error);
                body = JSON.parse(body);
                this.init(body.id);
                console.log('Create new Match');
                resolve();
            });
        });
    }

    startMatch() {
        const options = {
            method: 'PUT',
            url: `http://localhost:1337/match/${this.id}`,
            form: {
                isInProgress: true
            }
        };

        return new Promise((resolve, reject) => {
            request(options, error => {
                if (hasError(error)) reject(error);
                this.isInProgress = true;
                this.duration = new Date().getTime();
                this.listenMatch();
                console.log('Start match');
                resolve();
            });
        });
    }

    initNextMatch() {
        const options = {
            url: `http://localhost:1337/match?babyfoot=${this.babyId}&isFinished=false`
        };

        return new Promise((resolve, reject) => {
            request(options, (error, response, body) => {
                if (hasError(error)) reject(error);
                body = JSON.parse(body)[0];
                if (!body) return this.createNewMatch().then(() => resolve());
                this.id = body.id;
                this.scoreBlue = body.blueScore;
                this.scoreRed = body.redScore;
                console.log('Init match');
                resolve();
            });
        });
    };

    listenMatch() {
        io.socket.get('/api/match/current?babyfoot=' + this.babyId);
        this.onFinished();
    }

    onFinished() {
        io.socket.on('match', async response => {
            if (response.data.isFinished) {
                io.socket.removeAllListeners();
                await this.matchIsFinished();
                await this.createNewMatch();
                askForGoal();
            }
        });
    }
}

const match = new Match(1);